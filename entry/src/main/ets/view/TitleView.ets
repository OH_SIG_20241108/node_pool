/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Title } from '../data/TitleBean';

// 标题栏子组件
@Component
export struct TitleView {
  @ObjectLink title: Title;
  @State dividerLen: string = '30';
  private clickListener: (() => void) = () => {
  };

  aboutToAppear(): void {
    this.dividerLen = (this.title.name.length * 15).toString();
  }

  build() {
    Column() {
      Text(this.title.name)
        .fontColor(this.title.isSelected ? $r('app.color.tabs_title_selected_color') : $r('app.color.tabs_title_color'))
        .fontSize($r('app.float.tabs_font_size'))
        .onClick(() => {
          if (this.clickListener) {
            this.clickListener();
          }
        })
      if (this.title.isSelected) {
        Divider()
          .color($r('app.color.tabs_title_selected_color'))
          .strokeWidth('2')
          .width(this.dividerLen)
          .margin({top: $r('app.float.divider_top_margin')})
      }
    }
  }
}