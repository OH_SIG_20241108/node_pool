/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BreakpointType } from '../utils/BreakpointSystem';
import { ItemImageView as ItemTravelImageView } from './ItemTravelImageView';
import { ItemRaidersView as ItemRaidersView } from './ItemRaidersView';
import { ItemSceneView } from './ItemSceneView';
import { ItemVideoView as ItemTravelVideoView } from './ItemTravelVideoView';
import { ItemUniversalView } from './ItemUniversalView';
import { ItemRentView } from './ItemRentView';
import { ItemHotelView } from './ItemHotelView';
import { CommonConstants, ItemTypeConstants } from '../constants/CommonConstants';
import { BreakpointConstants } from '../constants/BreakpointConstants';
import { WaterFlowDataSource } from '../data/WaterFlowDataSource';
import {
  recommendData,
  surroundData,
  outTravelData,
  selfDriveData,
  mountainData,
  parentingData,
  freeWalkerData,
  campingData
} from '../data/MockData';
import { ViewItem } from '../data/ViewItem';

@Component
export struct WaterFlowView {
  @StorageLink(BreakpointConstants.BREAKPOINT_NAME) currentBreakpoint: string = BreakpointConstants.BREAKPOINT_SM;
  index: number = 0;
  dataSource: WaterFlowDataSource = new WaterFlowDataSource();

  aboutToAppear(): void {
    // 添加模拟数据
    switch (this.index) {
      case 0:
        this.dataSource.addItems(recommendData());
        break;
      case 1:
        this.dataSource.addItems(surroundData());
        break;
      case 2:
        this.dataSource.addItems(outTravelData());
        break;
      case 3:
        this.dataSource.addItems(selfDriveData());
        break;
      case 4:
        this.dataSource.addItems(mountainData());
        break;
      case 5:
        this.dataSource.addItems(parentingData());
        break;
      case 6:
        this.dataSource.addItems(freeWalkerData());
        break;
      case 7:
        this.dataSource.addItems(campingData());
        break;
    }
  }

  build() {
    Column({ space: CommonConstants.SPACE_EIGHT }) {
      Column() {
        WaterFlow() {
          LazyForEach(this.dataSource, (item: ViewItem, index: number) => {
            FlowItem() {
              if (item.type === ItemTypeConstants.TYPE_UNIVERSAL) {
                // 通用卡片
                ItemUniversalView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_UNIVERSAL);
              } else if (item.type === ItemTypeConstants.TYPE_RAIDERS) {
                // 攻略卡片
                ItemRaidersView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_RAIDERS);
              } else if (item.type === ItemTypeConstants.TYPE_TRAVEL_IMAGE) {
                // 旅行卡片1
                ItemTravelImageView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_TRAVEL_IMAGE);
              } else if (item.type === ItemTypeConstants.TYPE_TRAVEL_VIDEO) {
                // 旅行卡片2
                ItemTravelVideoView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_TRAVEL_VIDEO);
              } else if (item.type === ItemTypeConstants.TYPE_SCENE) {
                ItemSceneView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_SCENE);
              } else if (item.type === ItemTypeConstants.TYPE_HOTEL) {
                ItemHotelView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_HOTEL);
              } else if (item.type === ItemTypeConstants.TYPE_RENT) {
                ItemRentView({
                  item: item,
                })
                  .reuseId(ItemTypeConstants.TYPE_RENT);
              }
            }
            .width($r('app.string.nodepool_percent_100'))
            .backgroundColor(Color.White)
            .clip(true)
            .borderRadius($r('app.float.common_border_radius_7'))
          }, (index: string) => index);
        }
        .cachedCount(CommonConstants.WATER_FLOW_CACHED_COUNT)
        .nestedScroll({ scrollForward: NestedScrollMode.PARENT_FIRST, scrollBackward: NestedScrollMode.SELF_FIRST })
        .columnsTemplate(new BreakpointType({
          sm: BreakpointConstants.GRID_NUM_TWO,
          md: BreakpointConstants.GRID_NUM_THREE,
          lg: BreakpointConstants.GRID_NUM_FOUR
        }).getValue(this.currentBreakpoint))
        .columnsGap($r('app.float.waterflow_columns_gap'))
        .rowsGap($r('app.float.waterflow_rows_gap'))
        .layoutDirection(FlexDirection.Column)
        .itemConstraintSize({
          minWidth: CommonConstants.ZERO_PERCENT,
          maxWidth: CommonConstants.FULL_PERCENT,
          minHeight: CommonConstants.ZERO_PERCENT,
        });
      }
      .width(CommonConstants.FULL_PERCENT)
      .height(CommonConstants.FULL_PERCENT)
    }
    .height(CommonConstants.FULL_PERCENT)
    .margin({
      top: $r('app.float.margin_8'),
      left: new BreakpointType({
        sm: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_LEFT_SM,
        md: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_LEFT_MD,
        lg: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_LEFT_LG
      }).getValue(this.currentBreakpoint),
      right: new BreakpointType({
        sm: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_RIGHT_SM,
        md: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_RIGHT_MD,
        lg: BreakpointConstants.SEARCHBAR_AND_WATER_FLOW_MARGIN_RIGHT_LG
      }).getValue(this.currentBreakpoint)
    })
    .animation({
      duration: CommonConstants.ANIMATION_DURATION_TIME,
      curve: Curve.EaseOut,
      playMode: PlayMode.Normal
    });
  }
}