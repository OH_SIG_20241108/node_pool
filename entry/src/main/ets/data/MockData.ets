/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ViewItem } from './ViewItem'
import { ItemTypeConstants } from '../constants/CommonConstants'

const recommendTitle: string[] =
  ['请慢用~', '大刺猬避坑指南！',
    '大家都说勇敢的人先看世界', '大家都说勇敢的人先看世界',
    'GW之光11摩天轮', '经济实惠度假大澡堂',
    'MMM7'];

const recommendCoverImage: Resource[] =
  [$r('app.media.custom_reusable_waterFlow1'), $r('app.media.custom_reusable_waterFlow2'),
    $r('app.media.custom_reusable_waterFlow3'), $r('app.media.custom_reusable_waterFlow4'),
    $r('app.media.custom_reusable_waterFlow5'), $r('app.media.custom_reusable_waterFlow6'),
    $r('app.media.custom_reusable_waterFlow7')];

const recommendPositionTitle: string[] = ['大刺猬', '小刺猬', '巴拉', '巴拉', '江水', '大河', '00'];

const recommendUserIcon: Resource[] =
  [$r('app.media.user1_icon'), $r('app.media.user2_icon'),
    $r('app.media.user1_icon'), $r('app.media.user2_icon'),
    $r('app.media.user1_icon'), $r('app.media.user2_icon'),
    $r('app.media.user1_icon')];

const recommendUserName: string[] = ['呵呵姑娘', '达芙妮', 'Leelook', '苏苏', 'super', '小小', '大哥'];

const recommendLikeCount: number[] = [100, 100, 123, 321, 100, 100, 123];

const itemType: string[] =
  [ItemTypeConstants.TYPE_UNIVERSAL, ItemTypeConstants.TYPE_RAIDERS, ItemTypeConstants.TYPE_TRAVEL_IMAGE,
    ItemTypeConstants.TYPE_TRAVEL_VIDEO, ItemTypeConstants.TYPE_SCENE, ItemTypeConstants.TYPE_HOTEL,
    ItemTypeConstants.TYPE_RENT];

// 推荐
export function recommendData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = recommendCoverImage[i % recommendCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const surroundCoverImage: Resource[] =
  [$r('app.media.custom_reusable_avator1'), $r('app.media.custom_reusable_avator2'),
    $r('app.media.custom_reusable_avator3'), $r('app.media.custom_reusable_avator4'),
    $r('app.media.custom_reusable_avator5'), $r('app.media.custom_reusable_avator6'),
    $r('app.media.custom_reusable_avator1')];

// 周边
export function surroundData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = surroundCoverImage[i % surroundCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const outTravelCoverImage: Resource[] =
  [$r('app.media.custom_reusable_image1'), $r('app.media.custom_reusable_image2'),
    $r('app.media.custom_reusable_image3'), $r('app.media.custom_reusable_image4'),
    $r('app.media.custom_reusable_image5'), $r('app.media.custom_reusable_image6'),
    $r('app.media.custom_reusable_image1')];

// 出境游
export function outTravelData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = outTravelCoverImage[i % outTravelCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const selfDriveCoverImage: Resource[] =
  [$r('app.media.custom_reusable_image_1'), $r('app.media.custom_reusable_image_2'),
    $r('app.media.custom_reusable_image_3'), $r('app.media.custom_reusable_image_4'),
    $r('app.media.custom_reusable_image_1'), $r('app.media.custom_reusable_image_2'),
    $r('app.media.custom_reusable_image_3')];

// 自驾
export function selfDriveData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = selfDriveCoverImage[i % selfDriveCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const mountainCoverImage: Resource[] =
  [$r('app.media.custom_reusable_nature_1'), $r('app.media.custom_reusable_nature_2'),
    $r('app.media.custom_reusable_nature_3'), $r('app.media.custom_reusable_nature_4'),
    $r('app.media.custom_reusable_nature_1'), $r('app.media.custom_reusable_nature_2'),
    $r('app.media.custom_reusable_nature_3')];

// 登山
export function mountainData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = mountainCoverImage[i % mountainCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const parentingCoverImage: Resource[] =
  [$r('app.media.custom_reusable_product00'), $r('app.media.custom_reusable_product01'),
    $r('app.media.custom_reusable_product02'), $r('app.media.custom_reusable_product03'),
    $r('app.media.custom_reusable_product04'), $r('app.media.custom_reusable_product05'),
    $r('app.media.custom_reusable_product00')];

// 亲子
export function parentingData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = parentingCoverImage[i % parentingCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const freeWalkerCoverImage: Resource[] =
  [$r('app.media.custom_reusable_waterFlow1'), $r('app.media.custom_reusable_waterFlow2'),
    $r('app.media.custom_reusable_waterFlow3'), $r('app.media.custom_reusable_waterFlow4'),
    $r('app.media.custom_reusable_waterFlow5'), $r('app.media.custom_reusable_waterFlow6'),
    $r('app.media.custom_reusable_waterFlow7')];

// 自由行
export function freeWalkerData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = freeWalkerCoverImage[i % freeWalkerCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}

const campingCoverImage: Resource[] =
  [$r('app.media.custom_reusable_avator1'), $r('app.media.custom_reusable_avator2'),
    $r('app.media.custom_reusable_avator3'), $r('app.media.custom_reusable_avator4'),
    $r('app.media.custom_reusable_avator5'), $r('app.media.custom_reusable_avator6'),
    $r('app.media.custom_reusable_avator1')];

// 露营
export function campingData(): ViewItem[] {
  const data: ViewItem[] = [];
  for (let i = 0; i < 100; i++) {
    const viewItem: ViewItem = new ViewItem();
    viewItem.index = i;
    viewItem.type = itemType[i % itemType.length];
    viewItem.title = recommendTitle[i % recommendTitle.length];
    viewItem.coverImage = campingCoverImage[i % campingCoverImage.length];
    viewItem.positionIcon = $r('app.media.position_icon');
    viewItem.positionTitle = recommendPositionTitle[i % recommendPositionTitle.length];
    viewItem.userIcon = recommendUserIcon[i % recommendUserIcon.length];
    viewItem.userName = recommendUserName[i % recommendUserName.length];
    viewItem.likeIcon = $r('app.media.like_icon');
    viewItem.likeCount = recommendLikeCount[i % recommendLikeCount.length];
    viewItem.rankTitle = '00必打卡景点榜 No.8';
    viewItem.price = '140';
    viewItem.score = '4.7分';
    viewItem.comment = '3733条评论';
    viewItem.des = '含导游服务|最多35人团'
    data.push(viewItem);
  }
  return data;
}