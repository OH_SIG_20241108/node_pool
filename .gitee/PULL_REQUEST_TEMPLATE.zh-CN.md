**IssueNo**:

**Description**:

**Feature or Bugfix**:
- [ ] Feature
- [ ] Bugfix

**Binary Source**:
- [ ] No
- [ ] Yes

？？若涉及二进制文件，填写说明，若不涉及，删除掉此行。

**预测试**:
- [ ] Pass
- [ ] Fail
- [ ] 评估不涉及

？？若有用例及验证结果，填写在此处，若不涉及，删除掉此行。

**合入前自检**:
- [ ] 不涉及非法兼容性变更；若涉及，已通过相应评审。
- [ ] 不涉及性能或已进行性能测试且无劣化。
- [ ] 符合对应的编码规范。
- [ ] 不涉及文档更新，或已更新了文档。
- [ ] 针对可测试性要求，已增加必要的自测用例、合理的日志记录或Trace信息
- [ ] 不存在非法的文件引入，包括图片和代码等。
- [ ] 对用户呈现的界面已做国际化处理。