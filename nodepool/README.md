# node pool

**专门为OpenHarmony打造的一款全局自定义组件复用的三方库，致力于更高效、更轻便、更简单。**

## 简介

通过BuilderNode创建全局的自定义组件复用池，实现跨页面的组件复用。

## 特性说明

### 容器管理设计

- 节点种类限制：typeCount，默认值50，可自定义，饱和后拒绝新类型节点入池
- 单节点复用池大小限制：nodeSize，默认值100，可自定义，饱和后拒绝新节点入池

### 老化机制设计

- 规定一个阈值（默认24h，可自定义），超过该时间未被使用过的节点从复用池中清理掉

### 状态管理设计

- 自定义组件的更新遵循状态管理的更新机制。WrapperBuilder中直接使用的自定义组件其父组件为BuildNode对象。因此，更新子组件即WrappedBuilder中定义的自定义组件，需要遵循状态管理的定义讲相关的状态定义为@Prop或者@ObjectLink。
- 装饰器的选择请参考状态管理的装饰器规格结合应用开发需求进行选择。

### 触摸事件

- 向BuilderNode中绑定的组件注入触摸事件，实现事件的模拟转发。

## 下载安装

```
ohpm install @hadss/nodepool
```

## 使用说明

### 创建可复用的自定义组件

```typescript
import { NodeItem, CustomNodePoolFactory } from '@hadss/nodepool';

class Params {
  text: string = "this is a text";

  constructor(text: string) {
    this.text = text;
  }
}

@Builder
function ButtonBuilder(params: Params) {
  Column() {
    Button(`button ` + params.text)
      .borderWidth(2)
      .backgroundColor(Color.Orange)
      .width("100%")
      .height("100%")
      .gesture(
        TapGesture()
          .onAction((event: GestureEvent) => {
            console.log("TapGesture");
          })
      )
  }
  .width('100%')
  .height(300)
  .backgroundColor(Color.Gray)
}
```

### 创建Builder及复用池类型

```typescript
let btnBuilder: WrappedBuilder<ESObject> = wrapBuilder<ESObject>(ButtonBuilder);

const REUSE_VIEW_TYPE_SWIPER: string = 'reuse_type_swiper_';
```

### 创建一个CustomNodePoolFactory类型的对象，通过多例模式预创建NodePool复用池，同时通过单例模式获取创建NodePool组件复用池，根据传入的type类型查找复用池中是否存在可复用的组件，如果有则直接使用，如果没有则重新创建。使用NodeContainerProxy组件占位，从复用池NodePool中获取组件加载到页面中

```typescript
@Entry
@Component
struct Index {
  private nodePoolFactory: CustomNodePoolFactory = new CustomNodePoolFactory();
  private controller: NodeItem | undefined;
  private typeCfg: TypeReuseConfig = {
    type: REUSE_VIEW_TYPE_SWIPER,
    expirationTime: 30 * 60 * 1000, // 老化时间
    reuseCallback: this.reuseCallback,
    recycleCallback: this.recycleCallback
  }

  // 组件复用生命周期回调
  private reuseCallback(item: NodeItem): void {
    console.log('tag1', `reuseCallback, id:${item.id}`);
  }

  // 组件回收生命周期回调
  private recycleCallback(item: NodeItem): void {
    console.log('tag1', `recycleCallback, id:${item.id}`);
  }

  aboutToAppear(): void {
    this.nodePoolFactory.getCommonNodePool().setTypeReuseConfig(this.typeCfg);
    // 组件预创建
    let res = this.nodePoolFactory.getNodePool().preCreateNode(REUSE_VIEW_TYPE_SWIPER, {
      text: 'hello'
    }, btnBuilder, this.getUIContext());
    // 组件复用
    this.controller = this.nodePoolFactory.getCommonNodePool().getNode(REUSE_VIEW_TYPE_SWIPER, {
      text: 'hello'
    }, btnBuilder);
  }

  build() {
    Column() {
      NodeContainerProxy({ nodeItem: this.controller })
      Text("点击进行参数传递和触摸事件传递")
        .width('100%')
        .height(300)
        .backgroundColor(Color.Pink)
        .onTouch((event) => {
          if (event != undefined) {
            this.controller?.postTouchEvent(event); // 触摸事件传递
            this.controller?.node?.update(new Params("on update data")); // 参数传递
          }
        })
    }
  }
}
```
### web组件预渲染使用方法

<ol>
<li>构建wrapBuilder和获取nodepool与上述流程一致
<li>在web的上级页面进行web页面的预创建（具体页面根据业务场景设定）

```typescript
  aboutToAppear(): void {
    webData.uiContext = this.getUIContext();
    nodepool.preCreateWebNode(webData, warp);
  }
```

<li>在web组件页面调用getWebNode获取创建的节点

```typescript
item: NodeItem | undefined = nodepool.getWebNode(webData, warp);

Column() {
  NodeContainerProxy({ nodeItem: this.item })
}
```

<li>在web组件上开启onActive实现预渲染（只需要预启动效果可不做）

```typescript
  Web({ src: data.url, controller: data.controller })
    .width('100%')
    .height('100%')
    .geolocationAccess(false)
    .onPageBegin(() => {
      if (data.onActive) {
        data.controller.onActive()
      }
    })
```

</ol>

## 接口说明
### NodeContainerProxy组件
| 参数名称     | 入参内容                | 功能简介         |
|----------|---------------------|--------------|
| nodeItem | NodeItem, undefined | 节点Controller |

### CustomNodePoolFactory接口

| 接口名称              | 入参内容           | 功能简介    |
|-------------------|----------------|---------|
| getCommonNodePool | NA             | 获取单例节点池 |
| getNodePool       | NodePoolConfig | 获取多例节点池 |

### NodePoolConfig参数列表
| 参数名称            | 入参内容            | 功能简介               |
|-----------------|-----------------|--------------------|
| typeCount       | number          | 节点种类数量，默认值：50      |
| nodeSize        | number          | 某种节点数量，默认值：100     |
| expirationTime  | number          | 老化时间，默认值：24小时。单位ms |
| reuseCallback   | ReuseCallback   | 复用时回调函数            |
| recycleCallback | RecycleCallback | 回收时回调函数            |

### NodePool接口

| 接口名称               | 入参内容                                                                                  | 功能简介              |
|--------------------|---------------------------------------------------------------------------------------|-------------------|
| getNode            | type: string, data: ESObject, builder: WrappedBuilder<ESObject>                       | 获取节点Controller    |
| getWebNode         | data: WebData, builder?: WrappedBuilder<WebData[]>                                    | 获取web节点Controller |
| preCreateNode      | type: string, data: ESObject, builder: WrappedBuilder<ESObject>, uiContext: UIContext | 预创建节点进池           |
| preCreateWebNode   | data: WebData, builder?: WrappedBuilder<WebData[]>                                    | 预创建web节点进池        |
| setTypeReuseConfig | typeCfg: TypeReuseConfig                                                              | 设置不同类型节点的老化时间     |
| setRecycleCallback | recycleCallback: RecycleCallback                                                      | 设置回收回调            |
| setReuseCallback   | reuseCallback: ReuseCallback                                                          | 设置复用回调            |

### NodePool参数列表
| 参数名称    | 入参内容                     | 功能简介                    |
|---------|--------------------------|-------------------------|
| type    | string                   | 节点种类，可自定义               |
| data    | ESObject                 | 节点数据                    |
| builder | WrappedBuilder<ESObject> | 创建节点树所需的无状态UI方法@Builder |

### TypeReuseConfig参数列表
| 参数名称            | 入参内容          | 功能简介         |
|-----------------|---------------|--------------|
| type            | string        | 节点种类，可自定义    |
| expirationTime  | number        | 老化时间，可自定义    |
| reuseCallback   | ReuseCallback | 组件复用回调函数，可为空 |
| recycleCallback | ReuseCallback | 组件回收回调函数，可为空 |

### WebData参数列表

| 参数名称       | 入参内容              | 功能简介                |
|------------|-------------------|---------------------|
| url        | string            | url地址               |
| controller | WebviewController | webview控制器          |
| onActive   | boolean           | 是否进入前台激活状态，默认值:true |
| uiContext  | UIContext         | UI上下文实例             |
| data       | ESObject          | 业务参数，如：组件属性值        |

## 新增特性

1. 新增web节点适配，支持web预渲染能力

## 约束与限制

nodepool库在下述版本验证通过：<br>
<li>DevEco Studio：NEXT 5.0.0 Release（5.0.3.910), SDK: API 12(5.0.0.71)。
<li>DevEco Studio：NEXT Beta1（5.0.3.906), SDK: API12 (5.0.0.71)。

## 注意事项
暂不支持并发场景<br>
当前方案仅支持builder函数，如果使用库主要的工作在于将数组改为builder函数，工作量取决于组件发复杂度。<br>
不要重复挂载builderNode，重复挂载会导致异常，出现重复挂载时日志会有错误记录。若需要相同节点的重复挂载可使用多例节点池<br>
节点池是以栈的形式入池和出池的，在预创建组件需要先mock数据的场景下，用户在复用时需要注意数据是否需要调整顺序，防止数据和创建的组件未对齐。<br>
因无法保证每次跨页面复用时复用池中均有缓存，因此建议性能敏感场景下优先使用预创建复用方案。

## 参考资料

[sample使用举例](https://gitee.com/openharmony-sig/node_pool)

## 贡献代码

使用过程中发现任何问题都可以提 [issue](https://gitee.com/openharmony-sig/node_pool/issues)
给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/node_pool/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/node_pool/blob/master/nodepool/LICENSE) ，请自由的享受和参与开源。