/*
 *   Copyright (c) 2024 Huawei Device Co., Ltd.
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
import { webview } from '@kit.ArkWeb';

export class WebData {
  /**
   * url地址
   */
  private _url: string;

  /**
   * webview控制器
   */
  private _controller: WebviewController = new webview.WebviewController;

  /**
   * UIContext
   */
  private _uiContext: UIContext | undefined = undefined;

  /**
   * 是否进入前台激活状态
   */
  private _onActive: boolean = true;

  /**
   * 业务参数
   */
  private _data: ESObject;

  constructor(url: string) {
    this._url = url;
  }

  public set url(value: string) {
    this._url = value;
  }

  public get url(): string {
    return this._url;
  }

  public set controller(value: WebviewController) {
    this._controller = value;
  }

  public get controller(): WebviewController {
    return this._controller;
  }

  public set uiContext(value: UIContext | undefined) {
    this._uiContext = value;
  }

  public get uiContext(): UIContext | undefined {
    return this._uiContext;
  }

  public set onActive(value: boolean) {
    this._onActive = value;
  }

  public get onActive(): boolean {
    return this._onActive;
  }

  public set data(value: ESObject) {
    this._data = value;
  }

  public get data(): ESObject {
    return this._data;
  }
}