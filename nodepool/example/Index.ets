/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NodeItem, CustomNodePoolFactory, TypeReuseConfig } from '@hadss/nodepool';
import { NodeContainerProxy } from '@hadss/nodepool/src/main/ets/lib/NodeItem';

class Params {
  text: string = 'this is a text';

  constructor(text: string) {
    this.text = text;
  }
}

@Builder
function ButtonBuilder(params: Params) {
  Column() {
    Button(`button ` + params.text)
      .borderWidth(2)
      .backgroundColor(Color.Orange)
      .width('100%')
      .height('100%')
      .gesture(
        TapGesture()
          .onAction((event: GestureEvent) => {
            console.log('TapGesture');
          })
      )
  }
  .width('100%')
  .height(300)
  .backgroundColor(Color.Gray)
}

let btnBuilder: WrappedBuilder<ESObject> = wrapBuilder<ESObject>(ButtonBuilder);

const REUSE_VIEW_TYPE_SWIPER: string = 'reuse_type_swiper_';

@Entry
@Component
struct Index {
  private nodePoolFactory: CustomNodePoolFactory = new CustomNodePoolFactory();
  private controller: NodeItem | undefined;
  private typeCfg: TypeReuseConfig = {
    type: REUSE_VIEW_TYPE_SWIPER,
    expirationTime: 30 * 60 * 1000, // 老化时间
    reuseCallback: this.reuseCallback,
    recycleCallback: this.recycleCallback
  }

  // 组件复用生命周期回调
  private reuseCallback(item: NodeItem): void {
    console.log('tag1', `reuseCallback, id:${item.id}`);
  }

  // 组件回收生命周期回调
  private recycleCallback(item: NodeItem): void {
    console.log('tag1', `recycleCallback, id:${item.id}`);
  }

  aboutToAppear(): void {
    this.nodePoolFactory.getCommonNodePool().setTypeReuseConfig(this.typeCfg);
    // 组件复用
    this.controller = this.nodePoolFactory.getCommonNodePool().getNode(REUSE_VIEW_TYPE_SWIPER, {
      text: 'hello'
    }, btnBuilder);
  }

  build() {
    Column() {
      NodeContainerProxy({ nodeItem: this.controller })
      Text('点击进行参数传递和触摸事件传递')
        .width('100%')
        .height(300)
        .backgroundColor(Color.Pink)
        .onTouch((event) => {
          if (event != undefined) {
            this.controller?.postTouchEvent(event); // 触摸事件传递
            this.controller?.node?.update(new Params('on update data')); // 参数传递
          }
        })
    }
  }
}